# duckdns-update
## Description
A POSIX compliant shell script that updates the public IP in DuckDNS.

Depends on `curl`.

## Installation
First, install all the required files:

```
$> git clone https://gitlab.com/makilakixki/duckdns-update.git
$> cd duckdns-update
$> ./build.sh
#> tar -xvf duckdns-update_<VERSION>.tar.xz -C /
```

`INSTALL_DIR` and `CONFIG_DIR` can be overriden such as:

```
$> INSTALL_DIR=<BIN_PATH> CONFIG_DIR=<CONF_PATH> ./build.sh
```

By default:
- `INSTALL_DIR`: `/usr/local/bin`
- `CONFIG_DIR`: `/usr/local/etc`

After installing the files, edit the configuration file at `CONFIG_DIR/duckdns-updater.conf`.

Finally, start the systemd timer:
```
#> systemctl daemon-reload
#> systemctl enable --now duckdns-update.timer
```

Optionally, check if the timer is running:

```
#> systemctl list-timers
```

## Usage
The usage can be seen by executing the following command:

```
$> duckdns-update --help
Usage: duckdns-update [OPTION]
Update public IP in DuckDNS, by sending a request to the service.

Options:
    -h, --help      display this help and exit
    -v, --version   display version information and exit

More information in <https://gitlab.com/makilakixki/duckdns-update>
```

Additionally, an IP update can be forced by executing:

```
#> duckdns-update
```

## Troubleshooting
To see if the request was correct, execute:

```
$> cat /var/log/duckdns-update.log
OK      # If the output is 'KO' check the options of the config file, something went wrong
```

## License
This project is licensed under the GNU AGPLv3.