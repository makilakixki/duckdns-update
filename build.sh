#!/bin/sh

# Project defines
VERSION="0.1.0"

# Project paths
EXE_NAME="duckdns-update"
BUILD_DIR="build"
PACKAGE_NAME="${EXE_NAME}_${VERSION}"
SCRIPT_PATH="src/${EXE_NAME}"
CONFIG_PATH="conf/${EXE_NAME}.conf"
SYSTEMD_SERVICE_PATH="services/systemd/${EXE_NAME}.service"
SYSTEMD_TIMER_PATH="services/systemd/${EXE_NAME}.timer"

create_base_file_structure() {
    mkdir -p ${BUILD_DIR}${INSTALL_DIR:-"/usr/local/bin"}
    mkdir -p ${BUILD_DIR}${CONFIG_DIR:-"/usr/local/etc"}
    cp ${SCRIPT_PATH} ${BUILD_DIR}${INSTALL_DIR:-"/usr/local/bin"}
    cp ${CONFIG_PATH} ${BUILD_DIR}${CONFIG_DIR:-"/usr/local/etc"}
    chmod +x ${BUILD_DIR}${INSTALL_DIR:-"/usr/local/bin"}/${EXE_NAME}
}

create_file_structure_systemd() {
    mkdir -p ${BUILD_DIR}${SYSTEMD_DIR:-"/etc/systemd/system"}
    cp ${SYSTEMD_SERVICE_PATH} ${BUILD_DIR}${SYSTEMD_DIR:-"/etc/systemd/system"}
    cp ${SYSTEMD_TIMER_PATH} ${BUILD_DIR}${SYSTEMD_DIR:-"/etc/systemd/system"}
}

write_version() {
    sed -i "s/VERSION=.*/VERSION=\"${VERSION}\"/" ${BUILD_DIR}${INSTALL_DIR:-"/usr/local/bin"}/${EXE_NAME}
}

package_tar_xz() {
    write_version
    tar -cvf ${PACKAGE_NAME}.tar -C ${BUILD_DIR} .
    xz -z ${PACKAGE_NAME}.tar
}

clean() {
    rm -rf ${BUILD_DIR}
}

#############################################
################### MAIN ####################
#############################################
main() {
    create_base_file_structure
    create_file_structure_systemd
    package_tar_xz
    clean
}

main

